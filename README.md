zf2InitModule
=============

Zend Framework 2 Init Module
(NOTICE: Thanks to cesarcancino.com)

Remember add 'Init' line on config/autoload/application.config.php

in:

```
return array(
    'modules' => array(
        'Application',
        'Init' // <-- Add this line
    ),
    ...

```

